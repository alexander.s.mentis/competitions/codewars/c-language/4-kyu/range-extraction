#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char *range_extraction(const int *args, size_t n)
{
    char *result = "";
    size_t lo = 0;
  
    while (lo < n) {
        int start = args[lo];
        int ct = 1;
        int end;
        while (lo+ct < n && args[lo+ct] - args[lo+ct-1] <= 1) {
            end = args[lo+ct];
            ct++;
        }
        if (strcmp(result, "") != 0) {
          asprintf(&result, "%s,", result);
        }
        if (ct >= 3) {
          asprintf(&result, "%s%d-%d", result, start, end);
        } else if (ct == 2) {
          asprintf(&result, "%s%d,%d", result, start, end);
        } else {
          asprintf(&result, "%s%d", result, start);
        } 
        lo = lo+ct;
    }
      
    return result;
}